// menu-section
$(function () {
    $('.hamburger').on("click", function (event) {
        event.preventDefault();

        $(this).toggleClass("is-active");


        $(".left-section").toggleClass("navebar-show");

        setTimeout(function () {
            $('body').toggleClass("hiddenOverflow");
        }, 300);

    });

    $('.navbar-menu a').on('mouseover', function () {
        $(this).parent().siblings().not(this).addClass("opacity-custom");
    });
    $('.navbar-menu a').on('mouseout', function () {
        $(this).parent().siblings().not(this).removeClass("opacity-custom");
    });
});


/*=====================================================================
==========================  Owl Carousel  =========================
========================================================================*/
$('#owl-demo').owlCarousel({
    autoplay: true,
    lazyLoad: true,
    loop: true,
    margin: 20,
    responsiveClass: true,
    autoHeight: true,
    autoplayTimeout: 7000,
    smartSpeed: 800,
    nav: true,
    navText: ["<img src='img/left-icon.png' class='right-cirle-icon'>", "<img src='img/right-icon.png' class='left-cirle-icon'>"],
    navClass: ['owl-prev', 'owl-next'],
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});


// equalHeight-auto
$(function () {
    var maxHeight = 0;

    $(".boxSec-content").each(function(){
        if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
    });

    $(".boxSec-content").height(maxHeight);
});



// slide
const buttons = document.getElementsByTagName("button");

for (const button of buttons) {
    button.addEventListener("click", () => {
        var id = button.getAttribute("id");

        var layerClass = "." + id + "-layer";
        var layers = document.querySelectorAll(layerClass);
        for (const layer of layers) {
            layer.classList.toggle("active");
        }
    });
}
